﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Tasker.Tasks.Interface
{
    /// <summary>
    /// This Interface defines a single action which can be executed
    /// It should be bindable, in Order to get the Status text etc.
    /// </summary>
    [InheritedExport]
    public interface ITaskAction : INotifyPropertyChanged
    {
        #region Public Methods
        /// <summary>
        /// This Method executes the Task asynchronously
        /// </summary>
        /// <returns></returns>
        Task ExecuteAsync();

        #endregion Public Methods

        #region Public Properties

        /// <summary>
        /// The name of the type of this action
        /// </summary>
        string DisplayName { get; }

        /// <summary>
        /// A Flag which indicates the status
        /// </summary>
        bool IsExecuting { get; }

        /// <summary>
        /// The UI for further settings for the task
        /// </summary>
        UserControl SettingsControl { get; }

        /// <summary>
        /// the Actual Status for the Task if its executing
        /// </summary>
        string Status { get; }

        /// <summary>
        /// A Name given by the User to name this Task
        /// </summary>
        string UserFriendlyName { get; set; }

        #endregion Public Properties
    }
}