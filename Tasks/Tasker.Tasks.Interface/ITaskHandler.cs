﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

using System.ComponentModel.Composition;
using System.Threading.Tasks;

namespace Tasker.Tasks.Interface
{
    /// <summary>
    /// This Interface defines a Handler for handling the TaskLists
    /// For example loading and saving the TaskLists
    /// </summary>
    [InheritedExport]
    public interface ITaskHandler
    {
        #region Public Properties

        /// <summary>
        /// The DefaultExtension for this Handler
        /// </summary>
        string DefaultExtension { get; }


        /// <summary>
        /// The FilePattern for this handler
        /// </summary>
        string FilePattern { get; }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// This Method loads the tasklist from the given path
        /// asynchronously
        /// </summary>
        /// <param name="path">the Path to load from</param>
        /// <returns>the loaded TaskList</returns>
        Task<ITaskList> LoadTaskListAsync(string path);

        /// <summary>
        /// This method saves the tasklist at the path of the TaskList
        /// asynchronously
        /// </summary>
        /// <param name="taskList">The TaskList to save</param>
        /// <returns></returns>
        Task SaveTaskListAsync(ITaskList taskList);

        #endregion Public Methods
    }
}