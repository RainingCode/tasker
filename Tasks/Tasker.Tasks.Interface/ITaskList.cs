﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
// 
// 
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion

using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Threading.Tasks;

namespace Tasker.Tasks.Interface
{
    /// <summary>
    /// This class defines a whole TaskList
    /// It should be bindable in Order to get the ExecutionStatus,
    /// the Tasks etc.
    /// </summary>
    [InheritedExport]
    public interface ITaskList : INotifyPropertyChanged
    {
        #region Public Methods

        /// <summary>
        /// This Method executes the whole TaskList
        /// </summary>
        /// <returns></returns>
        Task ExecuteAsync();

        #endregion Public Methods

        #region Public Properties

        /// <summary>
        /// The path to the config
        /// </summary>
        string ConfigPath { get; set; }

        /// <summary>
        /// The current index of the executed TaskAction 
        /// </summary>
        int CurrentExecutionStep { get; set; }

        /// <summary>
        /// The Description of the actual Status
        /// </summary>
        string CurrentExecutionStepDescription { get; }

        /// <summary>
        /// A Name given by the User to name this TaskList
        /// </summary>
        string DisplayName { get; set; }


        /// <summary>
        /// Flag if its executing
        /// </summary>
        bool IsExecuting { get; }

        /// <summary>
        /// A Collection of TaskActions which belongs to  this List
        /// </summary>
        ObservableCollection<ITaskAction> TaskActions { get; set; }

        #endregion Public Properties
    }
}
