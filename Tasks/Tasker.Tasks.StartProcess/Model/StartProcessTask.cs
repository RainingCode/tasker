﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
// 
// 
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion

using System;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Controls;
using Microsoft.Practices.ServiceLocation;
using Newtonsoft.Json;
using Prism.Mvvm;
using Tasker.Tasks.Interface;
using Tasker.Tasks.StartProcess.Views;

namespace Tasker.Tasks.StartProcess.Model
{
    /// <summary>
    ///     This class defines a Task to start a given Process
    /// </summary>
    [Export(typeof(StartProcessTask))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class StartProcessTask : BindableBase, ITaskAction
    {
        #region Public Constructors

        public StartProcessTask()
        {
            this.Status = "Ready";
            this.SettingsControl = ServiceLocator.Current.GetInstance<SettingsView>();
        }

        #endregion Public Constructors

        #region Public Methods

        public async Task ExecuteAsync()
        {
            if (string.IsNullOrEmpty(this.FullExecutablePath) ||
                !File.Exists(this.FullExecutablePath))
            {
                this.Status = "Error: No or wrong path given.";
                return;
            }
            this.IsExecuting = true;
            this.Status = "Initializing...";
            await Task.Factory.StartNew(() =>
                                        {
                                            try
                                            {
                                                this.Status = string.Format("Starting {0}...", this.FullExecutablePath);
                                                Process.Start(this.FullExecutablePath, this.StartupArguments);
                                                this.Status = "Finished. Successful started the program";
                                            }
                                            catch (Exception e)
                                            {
                                                this.Status = string.Format("Error while starting the program: {0}",
                                                                            e.Message);
                                            }
                                        });
            this.IsExecuting = false;
        }

        #endregion Public Methods

        #region Private Fields

        private string _fullExecutablePath;
        private bool _isExecuting;
        private SettingsView _settingsControl;
        private string _startupArguments;
        private string _status;
        private string _userFriendlyName;

        #endregion Private Fields

        #region Public Properties

        public string DisplayName
        {
            get { return "Start Process"; }
        }

        /// <summary>
        /// The Full Path to the executable which should be executed
        /// </summary>
        public string FullExecutablePath
        {
            get { return this._fullExecutablePath; }
            set
            {
                if (this._fullExecutablePath != value)
                {
                    this._fullExecutablePath = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        public bool IsExecuting
        {
            get { return this._isExecuting; }
            set
            {
                if (this._isExecuting != value)
                {
                    this._isExecuting = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        public UserControl SettingsControl
        {
            get { return this._settingsControl; }
            set
            {
                var control = value as SettingsView;
                if (control == null)
                {
                    return;
                }
                this._settingsControl = control;
                this._settingsControl.SetTask(this);
            }
        }

        /// <summary>
        /// The Arguments to start the executable with
        /// </summary>
        public string StartupArguments
        {
            get { return this._startupArguments; }
            set
            {
                if (this._startupArguments != value)
                {
                    this._startupArguments = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        public string Status
        {
            get { return this._status; }
            set
            {
                if (this._status != value)
                {
                    this._status = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public string UserFriendlyName
        {
            get { return this._userFriendlyName; }
            set
            {
                if (this._userFriendlyName != value)
                {
                    this._userFriendlyName = value;
                    this.OnPropertyChanged();
                }
            }
        }

        #endregion Public Properties
    }
}
