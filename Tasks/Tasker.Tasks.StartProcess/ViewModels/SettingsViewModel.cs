﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
// 
// 
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion

using System.ComponentModel.Composition;
using System.Windows.Input;
using Prism.Commands;
using Prism.Mvvm;
using Tasker.Common.Interfaces.ProcessSelector;
using Tasker.Tasks.StartProcess.Model;

namespace Tasker.Tasks.StartProcess.ViewModels
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SettingsViewModel : BindableBase
    {
        #region Private Methods


        /// <summary>
        /// This Method opens the process Selector to choose
        /// from the running processes a executable with its 
        /// arguments
        /// </summary>
        private async void SelectByProcess()
        {
            var process = await this._processSelector.SelectProcessAsync();
            if (process == null)
            {
                return;
            }

            this.Task.FullExecutablePath = process.FullExecutablePath;
            this.Task.StartupArguments = process.StartupArguments;
        }

        #endregion Private Methods

        #region Private Fields

        /// <summary>
        /// The ProcessSelector to choose from
        /// </summary>
        [Import] private IProcessSelector _processSelector;

        private ICommand _selectByProcessCommand;

        private StartProcessTask _task;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// The bindable command to choose the executable 
        /// from a process
        /// </summary>
        public ICommand SelectByProcessCommand
        {
            get
            {
                return this._selectByProcessCommand ??
                       (this._selectByProcessCommand = new DelegateCommand(this.SelectByProcess));
            }
        }

        /// <summary>
        /// The TaskAction which belongs to this view
        /// </summary>
        public StartProcessTask Task
        {
            get { return this._task; }
            set
            {
                if (this._task != value)
                {
                    this._task = value;
                    this.OnPropertyChanged();
                }
            }
        }

        #endregion Public Properties
    }
}
