﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
// 
// 
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion

using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Prism.Mvvm;
using Tasker.Tasks.Interface;

namespace Tasker.Tasks.TaskList.Model
{
    /// <summary>
    /// This class defines a Handler for Handling JSON Files
    /// </summary>
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class TaskList : BindableBase, ITaskList
    { 

        #region Public Methods

        public async Task ExecuteAsync()
        {
            if (!this.TaskActions.Any() || this.IsExecuting)
            {
                return;
            }
            this.IsExecuting = true;
            this.CurrentExecutionStep = 0;
            this.CurrentExecutionStepDescription = "Initializing...";

            foreach (var taskAction in this.TaskActions)
            {
                this.CurrentExecutionStepDescription = string.Format("Executing {0} ({1})...",
                                                                     taskAction.UserFriendlyName,
                                                                     taskAction.DisplayName);

                await taskAction.ExecuteAsync();
                this.CurrentExecutionStep++;
            }
            this.CurrentExecutionStepDescription = "Finished.";
            this.IsExecuting = false;
        }

        #endregion Public Methods
        
        #region Private Fields

        private string _configPath;

        private int _currentExecutionStep;

        private string _currentExecutionStepDescription;

        private string _displayName;

        private bool _isExecuting;

        private ObservableCollection<ITaskAction> _taskActions;

        #endregion Private Fields

        #region Public Properties

        public string ConfigPath
        {
            get { return this._configPath; }
            set
            {
                if (this._configPath != value)
                {
                    this._configPath = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        public int CurrentExecutionStep
        {
            get { return this._currentExecutionStep; }
            set
            {
                if (this._currentExecutionStep != value)
                {
                    this._currentExecutionStep = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        public string CurrentExecutionStepDescription
        {
            get { return this._currentExecutionStepDescription; }
            private set
            {
                if (this._currentExecutionStepDescription != value)
                {
                    this._currentExecutionStepDescription = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public string DisplayName
        {
            get { return this._displayName; }
            set
            {
                if (this._displayName != value)
                {
                    this._displayName = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        public bool IsExecuting
        {
            get { return this._isExecuting; }
            private set
            {
                if (this._isExecuting != value)
                {
                    this._isExecuting = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public ObservableCollection<ITaskAction> TaskActions
        {
            get { return this._taskActions ?? (this.TaskActions = new ObservableCollection<ITaskAction>()); }
            set
            {
                if (this._taskActions != value)
                {
                    this._taskActions = value;
                    this.OnPropertyChanged();
                }
            }
        }

        #endregion Public Properties
    }
}
