﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
// 
// 
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion

using System;
using System.ComponentModel.Composition;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Tasker.Tasks.Interface;

namespace Tasker.Tasks.TaskHandler.Model
{
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class TaskHandler : ITaskHandler
    {
        #region Private Fields

        /// <summary>
        ///     The Settings for the serializer
        /// </summary>
        private static readonly JsonSerializerSettings SerializerSettings = new JsonSerializerSettings
                                                                            {
                                                                                NullValueHandling =
                                                                                    NullValueHandling.Ignore,
                                                                                TypeNameHandling = TypeNameHandling.All,
                                                                                Formatting = Formatting.Indented
                                                                            };

        #endregion Private Fields

        #region Public Properties

        public string DefaultExtension
        {
            get { return "json"; }
        }

        public string FilePattern
        {
            get { return "JSON File (*.json)|*.json"; }
        }

        #endregion Public Properties

        #region Public Methods

        public async Task<ITaskList> LoadTaskListAsync(string path)
        {
            ITaskList taskList = null;
            await Task.Factory.StartNew(() =>
                                        {
                                            try
                                            {
                                                var fileContent = File.ReadAllText(path);
                                                taskList = JsonConvert.DeserializeObject<ITaskList>(fileContent,
                                                                                                    SerializerSettings);
                                                // In case of file has been moved
                                                taskList.ConfigPath = path;
                                            }
                                            catch (Exception e)
                                            {
                                                // ToDo: Handle Exception
                                            }
                                        },
                                        CancellationToken.None,
                                        TaskCreationOptions.None,
                                        TaskScheduler.FromCurrentSynchronizationContext());

            return taskList;
        }
        
        
        /// <summary>
        /// <seealso cref="ITaskList"/>
        /// It has to be in a STAThread in order to get UI Content (bindable objects)
        /// </summary>
        /// <param name="taskList"></param>
        /// <returns></returns>
        [STAThread]
        public async Task SaveTaskListAsync(ITaskList taskList)
        {
            await Task.Factory.StartNew(() =>
                                        {
                                            try
                                            {
                                                var fileContent = JsonConvert.SerializeObject(taskList,
                                                                                              SerializerSettings);
                                                File.WriteAllText(taskList.ConfigPath, fileContent);
                                            }
                                            catch (Exception e)
                                            {
                                                // ToDo: Handle Exception
                                            }
                                        });
        }

        #endregion Public Methods
    }
}
