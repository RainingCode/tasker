﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
// 
// 
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion

using System;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Controls;
using Microsoft.Practices.ServiceLocation;
using Newtonsoft.Json;
using Prism.Mvvm;
using Tasker.Tasks.Interface;
using Tasker.Tasks.KillProcess.Views;

namespace Tasker.Tasks.KillProcess.Model
{

    /// <summary>
    /// This class defines a Task to Kill a given Process
    /// </summary>
    [Export(typeof(KillProcessTask))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class KillProcessTask : BindableBase, ITaskAction
    {
        #region Public Constructors

        public KillProcessTask()
        {
            this.Status = "Ready";
            this.SettingsControl = ServiceLocator.Current.GetInstance<SettingsView>();
        }

        #endregion Public Constructors

        #region Public Methods

        public async Task ExecuteAsync()
        {
            if (string.IsNullOrEmpty(this.ProcessName))
            {
                this.Status = "Error: No Processname given.";
                return;
            }
            this.IsExecuting = true;
            this.Status = "Initializing...";
            await Task.Factory.StartNew(() =>
                                        {
                                            this.Status = "Getting Processes...";
                                            var processes = Process.GetProcessesByName(this.ProcessName);
                                            var killCount = 0;
                                            foreach (var process in processes)
                                            {
                                                try
                                                {
                                                    this.Status = string.Format("Killing process with ID {0}",
                                                                                process.Id);
                                                    process.Kill();
                                                    this.Status = "Successful killed the process";
                                                    killCount++;
                                                }
                                                catch (Exception e)
                                                {
                                                    // ToDo: Handle Exception
                                                }
                                            }

                                            this.Status =
                                                string.Format("Successful killed {0} processes. {1} Errors occured.",
                                                              killCount,
                                                              processes.Length - killCount);
                                        });
            this.IsExecuting = false;
        }

        #endregion Public Methods

        #region Private Fields

        private bool _isExecuting;
        private string _processName;
        private SettingsView _settingsControl;
        private string _status;
        private string _userFriendlyName;

        #endregion Private Fields

        #region Public Properties

        public string DisplayName
        {
            get { return "Kill Process"; }
        }

        [JsonIgnore]
        public bool IsExecuting
        {
            get { return this._isExecuting; }
            set
            {
                if (this._isExecuting != value)
                {
                    this._isExecuting = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public string ProcessName
        {
            get { return this._processName; }
            set
            {
                if (this._processName != value)
                {
                    this._processName = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        public UserControl SettingsControl
        {
            get { return this._settingsControl; }
            set
            {
                var control = value as SettingsView;
                if (control == null)
                {
                    return;
                }
                this._settingsControl = control;
                this._settingsControl.SetTask(this);
            }
        }

        [JsonIgnore]
        public string Status
        {
            get { return this._status; }
            set
            {
                if (this._status != value)
                {
                    this._status = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public string UserFriendlyName
        {
            get { return this._userFriendlyName; }
            set
            {
                if (this._userFriendlyName != value)
                {
                    this._userFriendlyName = value;
                    this.OnPropertyChanged();
                }
            }
        }

        #endregion Public Properties
    }
}
