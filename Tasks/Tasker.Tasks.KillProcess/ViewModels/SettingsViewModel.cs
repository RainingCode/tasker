﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

using Prism.Commands;
using Prism.Mvvm;
using System.ComponentModel.Composition;
using System.Windows.Input;
using Tasker.Common.Interfaces.ProcessSelector;
using Tasker.Tasks.KillProcess.Model;

namespace Tasker.Tasks.KillProcess.ViewModels
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SettingsViewModel : BindableBase
    {
        #region Private Methods

        /// <summary>
        /// This Method opens the process selector
        /// and sets the selected Process
        /// </summary>
        private async void SelectProcess()
        {
            var process = await this._processSelector.SelectProcessAsync();

            this.Task.ProcessName = process == null ? string.Empty : process.ProcessName;
        }

        #endregion Private Methods

        #region Private Fields

        /// <summary>
        /// The process selector for selecting a process
        /// </summary>
        [Import] private IProcessSelector _processSelector;

        private ICommand _selectProcessCommand;
        private KillProcessTask _task;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// A bindable command to open the ProcessSelector
        /// </summary>
        public ICommand SelectProcessCommand
        {
            get
            {
                return this._selectProcessCommand ??
                       (this._selectProcessCommand = new DelegateCommand(this.SelectProcess));
            }
        }

        /// <summary>
        /// The Action which belongs to this ViewModel
        /// </summary>
        public KillProcessTask Task
        {
            get { return this._task; }
            set
            {
                if (this._task != value)
                {
                    this._task = value;
                    this.OnPropertyChanged();
                }
            }
        }

        #endregion Public Properties
    }
}