﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
// 
// 
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion

using System.ComponentModel.Composition;
using System.Windows;
using Tasker.Tasks.KillProcess.Model;
using Tasker.Tasks.KillProcess.ViewModels;

namespace Tasker.Tasks.KillProcess.Views
{
    /// <summary>
    ///     Interaction logic for SettingsView.xaml
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class SettingsView
    {
        #region Public Fields

        // Using a DependencyProperty as the backing store for ViewModel.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel",
                                        typeof(SettingsViewModel),
                                        typeof(SettingsView),
                                        new FrameworkPropertyMetadata(null,
                                                                      FrameworkPropertyMetadataOptions
                                                                          .BindsTwoWayByDefault));

        #endregion Public Fields

        #region Public Constructors

        public SettingsView()
        {
            this.InitializeComponent();
        }

        #endregion Public Constructors

        #region Public Properties

        /// <summary>
        /// The ViewModel of this View
        /// </summary>
        [Import]
        public SettingsViewModel ViewModel
        {
            get { return (SettingsViewModel) this.GetValue(ViewModelProperty); }
            set { this.SetValue(ViewModelProperty, value); }
        }

        #endregion Public Properties

        #region Internal Methods

        /// <summary>
        /// This Method sets the TaskAction to the ViewModel
        /// </summary>
        /// <param name="killProcessTask">the TaskAction to set</param>
        internal void SetTask(KillProcessTask killProcessTask)
        {
            this.ViewModel.Task = killProcessTask;
        }

        #endregion Internal Methods
    }
}
