﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
// 
// 
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion

using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using Dragablz;
using Microsoft.Practices.ServiceLocation;
using Prism.Mvvm;
using Tasker.Shell.Properties;
using Tasker.Tasks.Interface;

namespace Tasker.Shell.ViewModels
{
    [Export]
    public class ShellWindowViewModel : BindableBase
    {
        #region Public Constructors

        [ImportingConstructor]
        public ShellWindowViewModel(ITaskHandler taskHandler)
        {
            this.LoadedConfigs = new ObservableCollection<ITaskList>();
            Application.Current.Exit += (s, e) => this.SaveAllOpenedTabs();



            if (Settings.Default.LastOpenedConfigs == null || Settings.Default.LastOpenedConfigs.Count == 0)
            {
                // show a empty config
                var emptyConfig = this.NewItemFactory.Invoke();
                emptyConfig.DisplayName = "Empty";
                this.LoadedConfigs.Add(emptyConfig);
                this.ActiveConfig = emptyConfig;
                return;
            }
            // Load the last opened configs
            var configList = Settings.Default.LastOpenedConfigs;
            foreach (var file in configList)
            {
                taskHandler.LoadTaskListAsync(file).ContinueWith(task =>
                                                                 {
                                                                     Application.Current
                                                                                .Dispatcher
                                                                                .Invoke(() =>
                                                                                        {
                                                                                            this.LoadedConfigs.Add(task.Result);
                                                                                            if (this.ActiveConfig == null)
                                                                                            {
                                                                                                this.ActiveConfig = this.LoadedConfigs
                                                                                                                        .FirstOrDefault();
                                                                                            }
                                                                                        });
                                                                 });
            }

        }

        #endregion Public Constructors

        #region Internal Methods

        /// <summary>
        /// This method will save all opened TaskList Pathes to the settings
        /// to load them at startup again
        /// </summary>
        internal void SaveAllOpenedTabs()
        {
            var path = this.LoadedConfigs.Select(x => x.ConfigPath).Where(x => !string.IsNullOrEmpty(x)).ToArray();
            Settings.Default.LastOpenedConfigs = new StringCollection();
            Settings.Default.LastOpenedConfigs.AddRange(path);
            Settings.Default.Save();
        }

        #endregion Internal Methods

        #region Private Fields

        private ITaskList _activeConfig;

        private ObservableCollection<ITaskList> _loadedConfigs;

        private bool _settingsFlyoutIsOpen;

        private IInterTabClient _tabClient;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// The active Config which is selected
        /// </summary>
        public ITaskList ActiveConfig
        {
            get { return this._activeConfig; }
            set
            {
                if (this._activeConfig != value)
                {
                    this._activeConfig = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// All loaded configs
        /// </summary>
        public ObservableCollection<ITaskList> LoadedConfigs
        {
            get { return this._loadedConfigs; }
            set
            {
                if (this._loadedConfigs != value)
                {
                    this._loadedConfigs = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Function to create a new TaskList
        /// </summary>
        public Func<ITaskList> NewItemFactory
        {
            get { return () => ServiceLocator.Current.GetInstance<ITaskList>(); }
        }

        /// <summary>
        /// Bool which indicates if the flyout is opened or not
        /// </summary>
        public bool SettingsFlyoutIsOpen
        {
            get { return this._settingsFlyoutIsOpen; }
            set
            {
                if (this._settingsFlyoutIsOpen != value)
                {
                    this._settingsFlyoutIsOpen = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// The TabClient for Dragabalz
        /// </summary>
        public IInterTabClient TabClient
        {
            get { return this._tabClient ?? (this._tabClient = new DefaultInterTabClient()); }
        }

        #endregion Public Properties
    }
}
