﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
// 
// 
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion

using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Mvvm;
using Tasker.Controls.PathSelection.Model;
using Tasker.Controls.PathSelection.View;
using Tasker.Tasks.Interface;

namespace Tasker.Shell.ViewModels
{
    [Export]
    internal class ShellTabContentControlViewModel : BindableBase
    {
        #region Internal Fields

        /// <summary>
        /// The Selection control of the view to manually opening the SelectionView
        /// </summary>
        internal PathSelectionControl SelectionControl;

        #endregion Internal Fields

        #region Public Constructors

        public ShellTabContentControlViewModel()
        {
            this.AvailableTasks.CollectionChanged += this.AvailableTasksOnCollectionChanged;
        }

        #endregion Public Constructors

        #region Private Fields

        private ICommand _addTaskCommand;

        private ObservableCollection<ITaskAction> _availableTasks;

        private ICommand _executeTaskCommand;

        private ICommand _executeTaskListCommand;

        private ICommand _loadTaskListCommand;

        private ICommand _removeTaskCommand;

        private ICommand _saveTaskListCommand;

        private ITaskAction _selectedTaskAction;

        private ITaskHandler _taskHandler;

        private ITaskList _taskList;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// A bindable command to add a new Task to the TaskList
        /// </summary>
        public ICommand AddTaskCommand
        {
            get { return this._addTaskCommand ?? (this._addTaskCommand = new DelegateCommand(this.AddTask)); }
        }


        /// <summary>
        /// All available TaskActions in the application
        /// </summary>
        [ImportMany(typeof(ITaskAction))]
        public ObservableCollection<ITaskAction> AvailableTasks
        {
            get { return this._availableTasks ?? (this._availableTasks = new ObservableCollection<ITaskAction>()); }
            set
            {
                if (this._availableTasks != value)
                {
                    this._availableTasks = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// A binadble Command to execute a specific TaskAction
        /// </summary>
        public ICommand ExecuteTaskCommand
        {
            get
            {
                return this._executeTaskCommand ??
                       (this._executeTaskCommand = DelegateCommand<ITaskAction>.FromAsyncHandler(this.ExecuteTaskAsync));
            }
        }
        
        /// <summary>
        /// A binadble Command to execute a the whole TaskList
        /// </summary>
        public ICommand ExecuteTaskListCommand
        {
            get
            {
                return this._executeTaskListCommand ??
                       (this._executeTaskListCommand = DelegateCommand.FromAsyncHandler(this.ExecuteTaskList));
            }
        }

        /// <summary>
        /// A binadble Command to load the TaskList from a file
        /// </summary>
        public ICommand LoadTaskListCommand
        {
            get
            {
                return this._loadTaskListCommand ??
                       (this._loadTaskListCommand = DelegateCommand.FromAsyncHandler(this.LoadTaskList));
            }
        }


        /// <summary>
        /// A binadble Command to remove a TaskAction from the List
        /// </summary>
        public ICommand RemoveTaskCommand
        {
            get
            {
                return this._removeTaskCommand ??
                       (this._removeTaskCommand = new DelegateCommand<ITaskAction>(this.RemoveTask));
            }
        }

        /// <summary>
        /// A binadble Command to save the TaskList to a file
        /// </summary>
        public ICommand SaveTaskListCommand
        {
            get
            {
                return this._saveTaskListCommand ??
                       (this._saveTaskListCommand = DelegateCommand.FromAsyncHandler(this.SaveTaskList));
            }
        }


        /// <summary>
        /// The selected TaskAction which can be added
        /// </summary>
        public ITaskAction SelectedTaskAction
        {
            get { return this._selectedTaskAction; }
            set
            {
                if (this._selectedTaskAction != value)
                {
                    this._selectedTaskAction = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// The Handler for saving and loading the Tasks
        /// </summary>
        [Import]
        public ITaskHandler TaskHandler
        {
            get { return this._taskHandler; }
            set
            {
                if (this._taskHandler != value)
                {
                    this._taskHandler = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// The tasklist which belongs to this view
        /// </summary>
        public ITaskList TaskList
        {
            get { return this._taskList; }
            set
            {
                if (this._taskList != value)
                {
                    this._taskList = value;
                    this.OnPropertyChanged();
                }
            }
        }

        #endregion Public Properties

        #region Private Methods

        /// <summary>
        /// This method adds a new Task to the list based on the
        /// SelectedTaskAction (<see cref="SelectedTaskAction"/>)
        /// </summary>
        private void AddTask()
        {
            try
            {
                var newAction = ServiceLocator.Current.GetInstance(this.SelectedTaskAction.GetType()) as ITaskAction;
                this.TaskList.TaskActions.Add(newAction);
                this.TaskList.CurrentExecutionStep = this.TaskList.TaskActions.Count;
            }
            catch (Exception e)
            {
                // Todo: handle Exception
            }
        }

        /// <summary>
        /// This Method selects the first TaskAction as soon as the Collection has been changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="notifyCollectionChangedEventArgs"></param>
        private void AvailableTasksOnCollectionChanged(object sender,
                                                       NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            if (this.AvailableTasks == null || !this.AvailableTasks.Any())
            {
                return;
            }

            this.AvailableTasks.CollectionChanged -= this.AvailableTasksOnCollectionChanged;
            if (this.SelectedTaskAction == null)
            {
                this.SelectedTaskAction = this.AvailableTasks.FirstOrDefault();
            }
        }

        /// <summary>
        /// This Method executes asynchronously the given TaskAction
        /// </summary>
        /// <param name="action">The action to execute</param>
        /// <returns></returns>
        private async Task ExecuteTaskAsync(ITaskAction action)
        {
            if (action != null)
            {
                await action.ExecuteAsync();
            }
        }

        /// <summary>
        /// This Method executes the whole TaskList asynchronously
        /// </summary>
        /// <returns></returns>
        private async Task ExecuteTaskList()
        {
            // ToDo: Progress Status
            if (this.TaskList != null)
            {
                await this.TaskList.ExecuteAsync();
            }
        }

        /// <summary>
        /// This method loads the TaskList from the file asynchronously
        /// </summary>
        /// <returns></returns>
        private async Task LoadTaskList()
        {
            if (string.IsNullOrEmpty(this.TaskList.ConfigPath))
            {
                this.ShowTaskListPathSelector(PathSelectionMode.OpenFile);
            }
            if (string.IsNullOrEmpty(this.TaskList.ConfigPath))
            {
                return;
            }
            var tmp = await this.TaskHandler.LoadTaskListAsync(this.TaskList.ConfigPath);
            this.TaskList.TaskActions = tmp.TaskActions;
            this.TaskList.ConfigPath = tmp.ConfigPath;
            this.TaskList.DisplayName = tmp.DisplayName;
        }


        /// <summary>
        /// This method removes the given TaskAction from the TaskList
        /// </summary>
        /// <param name="action">The action to remove</param>
        private void RemoveTask(ITaskAction action)
        {
            // ToDo: Message to user
            if (this.TaskList != null && this.TaskList.TaskActions.Any() && this.TaskList.TaskActions.Contains(action))
            {
                this.TaskList.TaskActions.Remove(action);
            }
        }

        /// <summary>
        /// This method saves the TaskList asynchronously
        /// </summary>
        /// <returns></returns>
        private async Task SaveTaskList()
        {
            if (string.IsNullOrEmpty(this.TaskList.ConfigPath))
            {
                this.ShowTaskListPathSelector(PathSelectionMode.SaveFile);
            }
            if (string.IsNullOrEmpty(this.TaskList.ConfigPath))
            {
                return;
            }
            await this.TaskHandler.SaveTaskListAsync(this.TaskList);
        }

        /// <summary>
        /// This method opens the PathSelector with the given Mode
        /// </summary>
        /// <param name="mode">The mode for the PathSelector</param>
        private void ShowTaskListPathSelector(PathSelectionMode mode)
        {
            this.SelectionControl.OpenPathSelector(mode);
        }

        #endregion Private Methods
    }
}
