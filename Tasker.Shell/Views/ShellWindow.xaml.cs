﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

using MahApps.Metro.Controls;
using Prism.Events;
using System.ComponentModel.Composition;
using System.Windows;

namespace Tasker.Shell.Views
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    [Export(typeof(MetroWindow))]
    [Export(typeof(ShellWindow))]
    [Export(typeof(Window))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public partial class ShellWindow
    {

        #region Public Constructors

        public ShellWindow()
        {
            this.InitializeComponent();
        }

        #endregion Public Constructors
    }
}