﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

using Microsoft.Practices.ServiceLocation;
using System.Windows;
using System.Windows.Controls;
using Prism.Regions;
using Tasker.Shell.ViewModels;
using Tasker.Tasks.Interface;

namespace Tasker.Shell.Views
{
    /// <summary>
    ///     Interaction logic for TabControl.xaml
    /// </summary>
    public partial class ShellTabContentControl
    {
        #region Public Constructors

        public ShellTabContentControl()
        {
            this.InitializeComponent();
            RegionManager.SetRegionManager(this, new RegionManager());
            this.ViewModel = ServiceLocator.Current.GetInstance<ShellTabContentControlViewModel>();
            this.ViewModel.SelectionControl = this.TaskListPathSelectionControl;
            this.ViewModel.TaskList = this.DataContext as ITaskList;
        }

        #endregion Public Constructors

        #region Public Properties

        /// <summary>
        /// The TaskList for this View
        /// </summary>
        public ITaskList TaskList
        {
            get { return (ITaskList) this.GetValue(TaskListProperty); }
            set { this.SetValue(TaskListProperty, value); }
        }

        #endregion Public Properties

        #region Internal Properties

        /// <summary>
        /// The ViewModel of this Control
        /// </summary>
        internal ShellTabContentControlViewModel ViewModel
        {
            get { return (ShellTabContentControlViewModel) this.GetValue(ViewModelProperty); }
            set { this.SetValue(ViewModelProperty, value); }
        }

        #endregion Internal Properties

        #region Private Methods

        /// <summary>
        /// Handler if the TaskList has been changed to update
        /// the ViewModel
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnTaskListPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as ShellTabContentControl;
            if (control == null)
            {
                return;
            }
            control.ViewModel.TaskList = e.NewValue as ITaskList;
        }

        #endregion Private Methods

        #region Public Fields

        // Using a DependencyProperty as the backing store for TaskList.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TaskListProperty =
            DependencyProperty.Register("TaskList",
                                        typeof(ITaskList),
                                        typeof(ShellTabContentControl),
                                        new FrameworkPropertyMetadata(null,
                                                                      FrameworkPropertyMetadataOptions
                                                                          .BindsTwoWayByDefault,
                                                                      OnTaskListPropertyChanged));

        // Using a DependencyProperty as the backing store for DataContextDirty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel",
                                        typeof(ShellTabContentControlViewModel),
                                        typeof(ShellTabContentControl),
                                        new FrameworkPropertyMetadata(null,
                                                                      FrameworkPropertyMetadataOptions
                                                                          .BindsTwoWayByDefault));

        #endregion Public Fields
    }
}