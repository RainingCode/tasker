﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace Tasker.Shell.Behavior
{
    /// <summary>
    /// This Behavior scrolls automatically to the selected Item in a ListView
    /// </summary>
    public class ScrollIntoViewForListBox : Behavior<ListBox>
    {
        #region Private Methods

        /// <summary>
        ///     On Selection Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssociatedObject_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            var listBox = sender as ListBox;
            if (listBox == null || listBox.SelectedItem == null)
            {
                return;
            }
            listBox.Dispatcher.BeginInvoke(
                                           (Action)(() =>
                                                    {
                                                        listBox.UpdateLayout();
                                                        if (listBox.SelectedItem != null)
                                                        {
                                                            listBox.ScrollIntoView(listBox.SelectedItem);
                                                        }
                                                        else
                                                        {
                                                            try
                                                            {
                                                                var item =
                                                                        listBox.Items.GetItemAt(
                                                                                                listBox
                                                                                                    .SelectedIndex);
                                                                listBox.ScrollIntoView(item);
                                                            }
                                                            catch (Exception)
                                                            {
                                                                    // ignored
                                                                }
                                                        }
                                                    }));


        }

        #endregion Private Methods

        #region Protected Methods

        /// <summary>
        ///     When Beahvior is attached
        /// </summary>
        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.SelectionChanged += this.AssociatedObject_SelectionChanged;
        }

        /// <summary>
        ///     When behavior is detached
        /// </summary>
        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.SelectionChanged -= this.AssociatedObject_SelectionChanged;
        }

        #endregion Protected Methods
    }
}