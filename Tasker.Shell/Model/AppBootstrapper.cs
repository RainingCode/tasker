﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

using Prism.Mef;
using System;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;
using System.Windows;
using Tasker.Shell.Views;

namespace Tasker.Shell.Model
{
    /// <summary>
    /// The bootstrapper of this Application
    /// </summary>
    internal class AppBootstrapper : MefBootstrapper
    {
        #region Protected Methods

        protected override void ConfigureAggregateCatalog()
        {
            base.ConfigureAggregateCatalog();
            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(AppBootstrapper).Assembly));

            foreach (var file in Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll"))
            {
                try
                {
                    this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(Assembly.LoadFile(file)));
                    // ReSharper disable once LocalizableElement
                    Console.WriteLine("Loaded {0}.", file);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error loading {0}: {1}", file, e.Message);
                }
            }
        }

        protected override DependencyObject CreateShell()
        {
            return this.Container.GetExportedValue<ShellWindow>();
        }

        protected override void InitializeShell()
        {
            base.InitializeShell();

            Application.Current.MainWindow = (ShellWindow) this.Shell;
            Application.Current.MainWindow.Show();
        }

        #endregion Protected Methods
    }
}