# Tasker #

## What is Tasker? ##

Tasker is a Small Application which allows you to define custom Tasklists. This is just a bunch of commands which will be executed as soon as you clicked Execute. Currently there are two Tasks defined:

* Start Process
* Stop Process


## Extend ##

If you want feel free to extend this to write your own Task. Simply implement the IAction Interface and you are done. As soon as you drop the DLL in the Folder it will be loaded by prism and is selectable in the UI.

## Future ##

* Define Start Trigger
* Append Metro Style to the file Selection

## Getting Started ##

Download the latest build from the [Download-Section](https://bitbucket.org/RainingCode/tasker/downloads) and start Tasker.Shell.exe.

## Versions ##

### V 1.0.0.0 ###

The First release. It contains two tasks:

* Start Process
* Stop Process


## Thanks ##

* Thanks to Alexandr Zinchenko for the awsome Library "CalcBinding" which makes the UI developing much easier. Check it out [here](https://github.com/Alex141/CalcBinding)
* James Willock for the cool Tab Control called "Dragablz". Check it out [here](https://dragablz.net/)
* Jan Karger and Steven Kirk for the MetroDialogs. You can find them [here](https://github.com/punker76/gong-wpf-dragdrop)
* Jan Karger, Dennis Daume, Brendan Forster, Paul Jenkins, Jake Ginnivan and Alex Mitchell for the great Mahapps Metro Library. I love it. Check it out [here](http://mahapps.com/)
* James Newton-King for the great JSON.Net Library. It's a cool library for handling JSON files. Check it out [here](http://www.newtonsoft.com/json)
* Brian Langunas, Brian Noyes and the whole Team behind the PRISM Framework. It's a really powerful framework for developing modular applications. Check it out [here](https://github.com/PrismLibrary/)