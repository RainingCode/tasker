﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

using Prism.Mvvm;
using System.Windows.Media;

namespace Tasker.Views.Settings.ThemeSettings.Model
{
    /// <summary>
    /// This class represents one Application Theme
    /// </summary>
    public class ApplicationTheme : BindableBase
    {
        #region Private Fields

        private Brush _borderColorBrush;
        private Brush _colorBrush;
        private string _name;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// The Brush for the border of the Icon
        /// </summary>
        public Brush BorderColorBrush
        {
            get { return this._borderColorBrush; }
            set
            {
                if (this._borderColorBrush != value)
                {
                    this._borderColorBrush = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// The Brush for the Color of this Color
        /// </summary>
        public Brush ColorBrush
        {
            get { return this._colorBrush; }
            set
            {
                if (this._colorBrush != value)
                {
                    this._colorBrush = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// The Name of this Color
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set
            {
                if (this._name != value)
                {
                    this._name = value;
                    this.OnPropertyChanged();
                }
            }
        }

        #endregion Public Properties
    }
}