﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

using System.ComponentModel.Composition;
using Tasker.Views.Settings.ThemeSettings.ViewModel;

namespace Tasker.Views.Settings.ThemeSettings.View
{
    /// <summary>
    ///     Interaction logic for ConfigurationView.xaml
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public partial class ThemeSettingsView
    {
        #region Public Constructors

        public ThemeSettingsView()
        {
            this.InitializeComponent();
        }

        #endregion Public Constructors

        #region Private Properties

        [Import]
        private ThemeSettingsViewModel ViewModel
        {
            set { this.DataContext = value; }
        }

        #endregion Private Properties
    }
}