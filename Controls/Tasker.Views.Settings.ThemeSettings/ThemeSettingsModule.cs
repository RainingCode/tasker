﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;
using System.ComponentModel.Composition;
using Tasker.SharedResources.Constants;
using Tasker.Views.Settings.ThemeSettings.View;

namespace Tasker.Views.Settings.ThemeSettings
{
    [ModuleExport(typeof(ThemeSettingsModule))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ThemeSettingsModule : IModule
    {
        #region Private Fields

        private readonly IRegionManager _regionManager;

        #endregion Private Fields

        #region Public Constructors

        [ImportingConstructor]
        public ThemeSettingsModule(IRegionManager regionManager)
        {
            this._regionManager = regionManager;
        }

        #endregion Public Constructors

        #region Public Methods

        public void Initialize()
        {
            this._regionManager.RegisterViewWithRegion(RegionNames.SettingsRegion, typeof(ThemeSettingsView));
        }

        #endregion Public Methods
    }
}