﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

using MahApps.Metro;
using Prism.Mvvm;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Tasker.Views.Settings.ThemeSettings.Model;

namespace Tasker.Views.Settings.ThemeSettings.ViewModel
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ThemeSettingsViewModel : BindableBase
    {
        #region Public Constructors

        [ImportingConstructor]
        public ThemeSettingsViewModel()
        {
            this.InitDesigns();
        }

        #endregion Public Constructors

        #region Private Fields

        private ObservableCollection<ApplicationTheme> _availableAccents;

        private ObservableCollection<ApplicationTheme> _availableThemes;
        private ApplicationTheme _selectedAccent;
        private ApplicationTheme _selectedTheme;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// The Accents which are available
        /// </summary>
        public ObservableCollection<ApplicationTheme> AvailableAccents
        {
            get { return this._availableAccents; }
            set
            {
                if (this._availableAccents != value)
                {
                    this._availableAccents = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// The Themes which are available
        /// </summary>
        public ObservableCollection<ApplicationTheme> AvailableThemes
        {
            get { return this._availableThemes; }
            set
            {
                if (this._availableThemes != value)
                {
                    this._availableThemes = value;
                    this.OnPropertyChanged();
                    this.UpdateUi();
                }
            }
        }

        /// <summary>
        /// The selected Accent
        /// </summary>
        public ApplicationTheme SelectedAccent
        {
            get { return this._selectedAccent; }
            set
            {
                if (this._selectedAccent != value)
                {
                    this._selectedAccent = value;
                    this.OnPropertyChanged();
                    this.UpdateUi();
                }
            }
        }

        /// <summary>
        /// The selected Theme
        /// </summary>
        public ApplicationTheme SelectedTheme
        {
            get { return this._selectedTheme; }
            set
            {
                if (this._selectedTheme != value)
                {
                    this._selectedTheme = value;
                    this.OnPropertyChanged();
                    this.UpdateUi();
                }
            }
        }

        #endregion Public Properties

        #region Private Methods

        /// <summary>
        ///     This method will Init the Design Properties with all available designs
        /// </summary>
        private void InitDesigns()
        {
            var themes = ThemeManager.AppThemes.Select(a => new ApplicationTheme
                                                            {
                                                                Name = a.Name,
                                                                BorderColorBrush =
                                                                    a.Resources["BlackColorBrush"] as Brush,
                                                                ColorBrush = a.Resources["WhiteColorBrush"] as Brush
                                                            });
            this.AvailableThemes = new ObservableCollection<ApplicationTheme>(themes);

            var accents = ThemeManager.Accents.Select(a => new ApplicationTheme
                                                           {
                                                               Name = a.Name,
                                                               ColorBrush = a.Resources["AccentColorBrush"] as Brush
                                                           });

            this.AvailableAccents = new ObservableCollection<ApplicationTheme>(accents);

            var themeConfig = Properties.Settings.Default.ThemeSettings;
            var accentConfig = Properties.Settings.Default.AccentSettings;
            if (themeConfig != null)
            {
                this.SelectedTheme = this.AvailableThemes.FirstOrDefault(x => x.Name == themeConfig);
            }
            if (accentConfig != null)
            {
                this.SelectedAccent = this.AvailableAccents.FirstOrDefault(x => x.Name == accentConfig);
            }
        }

        /// <summary>
        ///     This Method will update the Theme of the Application and saves the selected theme.
        /// </summary>
        private void UpdateUi()
        {
            if (this.SelectedTheme == null || this.SelectedAccent == null)
            {
                return;
            }
            var appTheme = ThemeManager.GetAppTheme(this.SelectedTheme.Name);
            var accent = ThemeManager.GetAccent(this.SelectedAccent.Name);
            ThemeManager.ChangeAppStyle(Application.Current, accent, appTheme);

            Properties.Settings.Default.AccentSettings = this.SelectedAccent.Name;
            Properties.Settings.Default.ThemeSettings = this.SelectedTheme.Name;
            Properties.Settings.Default.Save();
        }

        #endregion Private Methods
    }
}