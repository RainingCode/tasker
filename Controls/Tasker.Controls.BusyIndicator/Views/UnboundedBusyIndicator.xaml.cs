﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
// 
// 
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion

using System.Windows;
using Tasker.Controls.BusyIndicator.Models;

namespace Tasker.Controls.BusyIndicator.Views
{
    /// <summary>
    ///     Interaction logic for UnboundedBusyIndicator.xaml
    /// </summary>
    public partial class UnboundedBusyIndicator
    {
        #region Public Constructors

        public UnboundedBusyIndicator()
        {
            this.InitializeComponent();
        }

        #endregion Public Constructors

        #region Internal Properties

        internal bool UseCircleIndicator
        {
            get { return (bool) this.GetValue(UseCircleIndicatorProperty); }
            set { this.SetValue(UseCircleIndicatorProperty, value); }
        }

        #endregion Internal Properties

        #region Private Methods

        private static void OnIndicatorPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var indicatorControl = d as UnboundedBusyIndicator;
            if (indicatorControl == null)
            {
                return;
            }
            indicatorControl.UseCircleIndicator = indicatorControl.Indicator == IndicatorType.Circle;
        }

        #endregion Private Methods

        #region Public Fields

        // Using a DependencyProperty as the backing store for BusyContent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BusyContentProperty =
            DependencyProperty.Register("BusyContent",
                                        typeof(object),
                                        typeof(UnboundedBusyIndicator),
                                        new FrameworkPropertyMetadata(null,
                                                                      FrameworkPropertyMetadataOptions
                                                                          .BindsTwoWayByDefault));

        // Using a DependencyProperty as the backing store for Indicator.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IndicatorProperty =
            DependencyProperty.Register("Indicator",
                                        typeof(IndicatorType),
                                        typeof(UnboundedBusyIndicator),
                                        new FrameworkPropertyMetadata(IndicatorType.Circle,
                                                                      FrameworkPropertyMetadataOptions
                                                                          .BindsTwoWayByDefault,
                                                                      OnIndicatorPropertyChanged));

        // Using a DependencyProperty as the backing store for IsBusy.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsBusyProperty =
            DependencyProperty.Register("IsBusy",
                                        typeof(bool),
                                        typeof(UnboundedBusyIndicator),
                                        new FrameworkPropertyMetadata(false,
                                                                      FrameworkPropertyMetadataOptions
                                                                          .BindsTwoWayByDefault));

        // Using a DependencyProperty as the backing store for NonBusyContent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NonBusyContentProperty =
            DependencyProperty.Register("NonBusyContent",
                                        typeof(object),
                                        typeof(UnboundedBusyIndicator),
                                        new FrameworkPropertyMetadata(null,
                                                                      FrameworkPropertyMetadataOptions
                                                                          .BindsTwoWayByDefault));

        // Using a DependencyProperty as the backing store for UseCircleIndicator.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UseCircleIndicatorProperty =
            DependencyProperty.Register("UseCircleIndicator",
                                        typeof(bool),
                                        typeof(UnboundedBusyIndicator),
                                        new FrameworkPropertyMetadata(true,
                                                                      FrameworkPropertyMetadataOptions
                                                                          .BindsTwoWayByDefault));

        #endregion Public Fields

        #region Public Properties

        /// <summary>
        ///     The content which will be shown if the Control is busy
        /// </summary>
        public object BusyContent
        {
            get { return this.GetValue(BusyContentProperty); }
            set { this.SetValue(BusyContentProperty, value); }
        }

        /// <summary>
        ///     The Type of the Indicator <seealso cref="IndicatorType" />
        /// </summary>
        public IndicatorType Indicator
        {
            get { return (IndicatorType) this.GetValue(IndicatorProperty); }
            set { this.SetValue(IndicatorProperty, value); }
        }

        /// <summary>
        ///     Flag if its busy or not
        /// </summary>
        public bool IsBusy
        {
            get { return (bool) this.GetValue(IsBusyProperty); }
            set { this.SetValue(IsBusyProperty, value); }
        }

        /// <summary>
        ///     The Content if its not busy
        /// </summary>
        public object NonBusyContent
        {
            get { return this.GetValue(NonBusyContentProperty); }
            set { this.SetValue(NonBusyContentProperty, value); }
        }

        #endregion Public Properties
    }
}
