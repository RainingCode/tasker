﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
// 
// 
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion

using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Tasker.Controls.PathSelection.Model;
using Tasker.Controls.PathSelection.ViewModel;

namespace Tasker.Controls.PathSelection.View
{
    /// <summary>
    ///     Interaction logic for PathSelectionControl.xaml
    /// </summary>
    public partial class PathSelectionControl
    {
        #region Internal Fields

        public static readonly DependencyProperty DefaultExtensionProperty =
            DependencyProperty.Register("DefaultExtension",
                                        typeof(string),
                                        typeof(PathSelectionControl),
                                        new FrameworkPropertyMetadata(string.Empty,
                                                                      FrameworkPropertyMetadataOptions
                                                                          .BindsTwoWayByDefault,
                                                                      OnDefaultExtensionPropertyChanged));

        public static readonly DependencyProperty FilePatternProperty =
            DependencyProperty.Register("FilePattern",
                                        typeof(string),
                                        typeof(PathSelectionControl),
                                        new FrameworkPropertyMetadata(string.Empty,
                                                                      FrameworkPropertyMetadataOptions
                                                                          .BindsTwoWayByDefault,
                                                                      FilePatternPropertyChanged));

        public static readonly DependencyProperty SelectedPathProperty =
            DependencyProperty.Register("SelectedPath",
                                        typeof(string),
                                        typeof(PathSelectionControl),
                                        new FrameworkPropertyMetadata(string.Empty,
                                                                      FrameworkPropertyMetadataOptions
                                                                          .BindsTwoWayByDefault,
                                                                      OnSelectedPathChanged));

        public static readonly DependencyProperty SelectionKindProperty =
            DependencyProperty.Register("SelectionKind",
                                        typeof(PathSelectionMode),
                                        typeof(PathSelectionControl),
                                        new FrameworkPropertyMetadata(PathSelectionMode.OpenFile,
                                                                      FrameworkPropertyMetadataOptions
                                                                          .BindsTwoWayByDefault,
                                                                      OnSelectionKindChanged));

        public static readonly DependencyProperty WatermarkTextProperty =
            DependencyProperty.Register("WatermarkText",
                                        typeof(string),
                                        typeof(PathSelectionControl),
                                        new FrameworkPropertyMetadata(string.Empty,
                                                                      FrameworkPropertyMetadataOptions
                                                                          .BindsTwoWayByDefault,
                                                                      OnWatermarkPropertyChanged));

        internal static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel",
                                        typeof(PathSelectionViewModel),
                                        typeof(PathSelectionControl),
                                        new FrameworkPropertyMetadata(null,
                                                                      FrameworkPropertyMetadataOptions
                                                                          .BindsTwoWayByDefault));

        #endregion Internal Fields

        #region Public Constructors

        public PathSelectionControl()
        {
            this.InitializeComponent();
            this.ViewModel = new PathSelectionViewModel();
        }

        #endregion Public Constructors

        #region Internal Properties

        internal PathSelectionViewModel ViewModel
        {
            get { return (PathSelectionViewModel) this.GetValue(ViewModelProperty); }
            set { this.SetValue(ViewModelProperty, value); }
        }

        #endregion Internal Properties
       

        #region Public Properties

        /// <summary>
        /// The default Extension for the Selection
        /// Only used when you have set the SelectionKind (<see cref="SelectionKind"/>)
        /// to SaveFile or OpenFile
        /// </summary>
        public string DefaultExtension
        {
            get { return (string) this.GetValue(DefaultExtensionProperty); }
            set { this.SetValue(DefaultExtensionProperty, value); }
        }

        /// <summary>
        /// The file pattern for the selection       
        /// Only used when you have set the SelectionKind (<see cref="SelectionKind"/>)
        /// to SaveFile or OpenFile
        /// </summary>
        public string FilePattern
        {
            get { return (string) this.GetValue(FilePatternProperty); }
            set { this.SetValue(FilePatternProperty, value); }
        }

        /// <summary>
        /// The path which the user has selected
        /// </summary>
        public string SelectedPath
        {
            get { return (string) this.GetValue(SelectedPathProperty); }
            set { this.SetValue(SelectedPathProperty, value); }
        }

        /// <summary>
        /// The Kind of selection <seealso cref="PathSelectionMode"/>
        /// </summary>
        public PathSelectionMode SelectionKind
        {
            get { return (PathSelectionMode) this.GetValue(SelectionKindProperty); }
            set { this.SetValue(SelectionKindProperty, value); }
        }

        /// <summary>
        /// The Text for the Watermark
        /// </summary>
        public string WatermarkText
        {
            get { return (string) this.GetValue(WatermarkTextProperty); }
            set { this.SetValue(WatermarkTextProperty, value); }
        }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// This Method opens the window for the PathSelection
        /// with the selected SelectionKind
        /// </summary>
        public void OpenPathSelector()
        {
            this.ViewModel.OpenPathSelector();
        }

        /// <summary>
        /// This Method opens the window for the PathSelection
        /// with the given SelectionKind
        /// <param name="mode">the mode for the Selection</param>
        /// </summary>
        public void OpenPathSelector(PathSelectionMode mode)
        {
            this.ViewModel.OpenPathSelector(mode);
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Handler for changes of the Filepattern to
        /// update the ViewModel
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void FilePatternPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var selectionControl = d as PathSelectionControl;
            if (selectionControl == null)
            {
                return;
            }

            selectionControl.ViewModel.Filter = e.NewValue != null ? e.NewValue.ToString() : string.Empty;
        }

        /// <summary>
        /// Handler for changes of the DefaultExtension to
        /// update the ViewModel
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnDefaultExtensionPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var selectionControl = d as PathSelectionControl;
            if (selectionControl == null)
            {
                return;
            }

            selectionControl.ViewModel.DefaultExtension = e.NewValue != null ? e.NewValue.ToString() : string.Empty;
        }


        /// <summary>
        /// Handler for changes of the SelectionKind to
        /// update the ViewModel
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnSelectionKindChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var selectionControl = d as PathSelectionControl;
            if (selectionControl == null)
            {
                return;
            }

            selectionControl.ViewModel.SelectionMode =
                (e.NewValue as PathSelectionMode?).GetValueOrDefault(PathSelectionMode.OpenFile);
        }

        /// <summary>
        /// Handler for changes of the SelectedPath to
        /// update the ViewModel
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnSelectedPathChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var selectionControl = d as PathSelectionControl;
            if (selectionControl == null)
            {
                return;
            }

            selectionControl.FileTextBoxName.Text = e.NewValue != null ? e.NewValue.ToString() : string.Empty;
        }


        /// <summary>
        /// Handler for changes of the Watermark to
        /// update the ViewModel
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnWatermarkPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var selectionControl = d as PathSelectionControl;
            if (selectionControl == null)
            {
                return;
            }

            selectionControl.ViewModel.WatermarkText = e.NewValue != null ? e.NewValue.ToString() : string.Empty;
        }

        /// <summary>
        /// Handler when the user double clicks on the textbox
        /// to open the Selection Dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FileTextBoxName_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.OpenPathSelector();
        }


        /// <summary>
        /// Handler for changes of the SelectedPathTextBox to
        /// update the SelectedPath Property
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private void FileTextBoxName_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.SelectedPath != null && this.SelectedPath.Equals(this.FileTextBoxName.Text))
            {
                return;
            }
            this.SelectedPath = this.FileTextBoxName.Text;
        }

        #endregion Private Methods
    }
}
