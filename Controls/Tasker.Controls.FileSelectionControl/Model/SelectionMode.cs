﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

namespace Tasker.Controls.PathSelection.Model
{
    /// <summary>
    /// This enum is for selecting the Mode of the PathSelector
    /// </summary>
    public enum PathSelectionMode
    {
        /// <summary>
        /// Use this for opening a File
        /// </summary>
        OpenFile,
        /// <summary>
        /// Use this for saving a File
        /// </summary>
        SaveFile,
        /// <summary>
        /// Use this for choosing a folder
        /// </summary>
        Folder
    }
}