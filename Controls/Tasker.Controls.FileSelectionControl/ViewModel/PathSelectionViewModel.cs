﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

using Metro.Dialogs;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Windows.Input;
using Tasker.Controls.PathSelection.Model;

namespace Tasker.Controls.PathSelection.ViewModel
{
    internal class PathSelectionViewModel : BindableBase
    {
        #region Internal Fields
        
        /// <summary>
        /// The filter of the Selection View
        /// </summary>
        internal string Filter;

        /// <summary>
        /// The Mode of the Selection
        /// </summary>
        internal PathSelectionMode SelectionMode;

        /// <summary>
        /// The default Extension of the selection
        /// </summary>
        internal string DefaultExtension;
        #endregion Internal Fields

        #region Private Fields

        private ICommand _browseFileCommand;
        private string _selectedFileName;
        private string _watermarkText;

        #endregion Private Fields

        #region Public Properties


        /// <summary>
        /// The bindable command to open the selection
        /// </summary>
        public ICommand BrowseFileCommand
        {
            get
            {
                return this._browseFileCommand ?? (this._browseFileCommand = new DelegateCommand(this.OpenPathSelector));
            }
        }
        
        /// <summary>
        /// The selected filename
        /// </summary>
        public string SelectedFileName
        {
            get { return this._selectedFileName; }
            set
            {
                if (this._selectedFileName != value)
                {
                    this._selectedFileName = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// The text for the watermark
        /// </summary>
        public string WatermarkText
        {
            get { return this._watermarkText; }
            set
            {
                if (this._watermarkText != value)
                {
                    this._watermarkText = value;
                    this.OnPropertyChanged();
                }
            }
        }

        #endregion Public Properties

        #region Internal Methods

        /// <summary>
        /// This method opens the selection mode with the SelectionMode
        /// which was set  (<seealso cref="SelectionMode"/>
        /// </summary>
        internal void OpenPathSelector()
        {
            this.OpenPathSelector(this.SelectionMode);
        }

        /// <summary>
        /// This Method opens the selection with the given mode
        /// </summary>
        /// <param name="mode">the mode of the Selector</param>
        internal void OpenPathSelector(PathSelectionMode mode)
        {
            var d = new WindowsDialogs(null);
            var newPath = string.Empty;

            switch (mode)
            {
                case PathSelectionMode.OpenFile:
                    newPath = d.ShowOpenFileDialog(null, this.Filter);
                    break;

                case PathSelectionMode.SaveFile:
                    newPath = d.ShowSaveFileDialog(null, this.Filter, this.Filter);
                    break;

                case PathSelectionMode.Folder:
                    newPath = d.ShowSelectFolderDialog(null, this.SelectedFileName);
                    break;

                default:
                    break;
            }

            if (string.IsNullOrEmpty(newPath))
            {
                return;
            }

            if (!newPath.EndsWith(this.DefaultExtension, StringComparison.InvariantCultureIgnoreCase) &&
                (this.SelectionMode == PathSelectionMode.OpenFile || this.SelectionMode == PathSelectionMode.SaveFile))
            {
                newPath += string.Format(".{0}", this.DefaultExtension);
            }

            this.SelectedFileName = newPath;
        }

        #endregion Internal Methods
    }
}