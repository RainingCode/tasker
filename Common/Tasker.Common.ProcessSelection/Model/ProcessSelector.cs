﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
// 
// 
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion

using System.ComponentModel.Composition;
using System.Threading.Tasks;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Tasker.Common.Interfaces.ProcessSelector;
using Tasker.Common.ProcessSelection.Views;

namespace Tasker.Common.ProcessSelection.Model
{
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class ProcessSelector : IProcessSelector
    {
        #region Public Methods

        public async Task<IProcess> SelectProcessAsync()
        {
            if (this._customDialog == null)
            {
                this._customDialog = new CustomDialog {Content = this._view, Title = "Select a process"};
                this._view.SetAsyncCloseMethod(this.CloseDialogAsync);
            }
            var settings = new MetroDialogSettings();
            await this._view.UpdateProcessesAsTask();
            await this._mainWindow.ShowMetroDialogAsync(this._customDialog);
            await this._customDialog.WaitUntilUnloadedAsync();
            return this._view.GetSelectedProcess();
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        ///     Handle to close the Dialog
        /// </summary>
        private async Task CloseDialogAsync()
        {
            await this._mainWindow.HideMetroDialogAsync(this._customDialog);
        }

        #endregion Private Methods

        #region Private Fields

        /// <summary>
        ///     The Dialog to show up for reuse
        /// </summary>
        private CustomDialog _customDialog;

        /// <summary>
        ///     The MainWindow to call MahApps Methods
        /// </summary>
        [Import] private MetroWindow _mainWindow;

        /// <summary>
        ///     The view of the ProcessSelector
        /// </summary>
        [Import] private ProcessSelectionView _view;

        #endregion Private Fields
    }
}
