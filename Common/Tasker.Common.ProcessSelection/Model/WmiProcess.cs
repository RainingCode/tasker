﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

using Tasker.Common.Interfaces.ProcessSelector;

namespace Tasker.Common.ProcessSelection.Model
{
    /// <summary>
    /// Implementation of IProcess Interface <see cref="IProcess"/>
    /// </summary>
    public class WmiProcess : IProcess
    {
        #region Public Properties

        public string FullExecutablePath { get; internal set; }

        public int ProcessId { get; internal set; }

        public string ProcessName { get; internal set; }

        public string StartupArguments { get; internal set; }

        #endregion Public Properties
    }
}