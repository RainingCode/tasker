﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

namespace Tasker.Common.ProcessSelection.Extensions
{
    /// <summary>
    /// This class holds the object extension for a ToStringMethod
    /// </summary>
    public static class ObjectToStringExtension
    {
        #region Public Methods

        /// <summary>
        /// This Extension allows to use toString on null objects
        /// if the value is null, then the default Value will be returned
        /// otherwise the toString method will be called
        /// </summary>
        /// <param name="obj">The object which should be converted to string</param>
        /// <param name="defaultString">the default value if the object is empty</param>
        /// <returns></returns>
        public static string ToStringOrDefault(this object obj, string defaultString = "")
        {
            if (obj == null)
            {
                return defaultString;
            }
            return obj.ToString();
        }

        #endregion Public Methods
    }
}