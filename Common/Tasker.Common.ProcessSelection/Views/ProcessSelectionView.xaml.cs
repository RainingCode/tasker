﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

using System;
using System.ComponentModel.Composition;
using System.Threading.Tasks;
using System.Windows;
using Tasker.Common.ProcessSelection.Model;
using Tasker.Common.ProcessSelection.ViewModels;

namespace Tasker.Common.ProcessSelection.Views
{
    /// <summary>
    ///     Interaction logic for ProcessSelectionView.xaml
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class ProcessSelectionView
    {
        /// <summary>
        /// The Height of the View in Percentage relative to the window
        /// </summary>
        private const double PercentageOfMainWindow = 0.7;
        #region Public Constructors

        [ImportingConstructor]
        public ProcessSelectionView(Window mainWindow)
        {
            this.InitializeComponent();
            mainWindow.SizeChanged += (s, e) =>
                                      {
                                          this.Height = mainWindow.Height * PercentageOfMainWindow;
                                      };

            this.Height = mainWindow.Height * PercentageOfMainWindow;
        }

        #endregion Public Constructors

        #region Internal Methods

        /// <summary>
        /// This Method gets the process which is selected by the user
        /// </summary>
        /// <returns>the selected process</returns>
        internal WmiProcess GetSelectedProcess()
        {
            var viewModel = this.DataContext as ProcessSelectionViewModel;
            return viewModel != null ? viewModel.SelectedProcess : null;
        }

        /// <summary>
        /// This Method sets the close handle for the view
        /// </summary>
        /// <param name="closeAction">the handle for closing the View</param>
        internal void SetAsyncCloseMethod(Func<Task> closeAction)
        {
            var viewModel = this.DataContext as ProcessSelectionViewModel;
            if (viewModel != null)
            {
                viewModel.SetAsyncCloseMethod(closeAction);
            }
        }

        /// <summary>
        /// This Method lets the ViewModel updating the running processes
        /// </summary>
        /// <returns></returns>
        internal async Task UpdateProcessesAsTask()
        {
            var viewModel = this.DataContext as ProcessSelectionViewModel;
            if (viewModel != null)
            {
                await viewModel.RefreshProcessListAsync();
            }
        }

        #endregion Internal Methods

    }
}