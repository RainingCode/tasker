﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
// 
// 
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Management;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Mvvm;
using Tasker.Common.ProcessSelection.Extensions;
using Tasker.Common.ProcessSelection.Model;

namespace Tasker.Common.ProcessSelection.ViewModels
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    internal class ProcessSelectionViewModel : BindableBase
    {
        #region Public Methods

        /// <summary>
        ///     Sets the close Handle for closing the Dialog
        /// </summary>
        /// <param name="closeAction">handle for closing</param>
        public void SetAsyncCloseMethod(Func<Task> closeAction)
        {
            this._closeAction = closeAction;
        }

        #endregion Public Methods

        #region Internal Methods

        /// <summary>
        ///     Refreshs the ProcessList
        /// </summary>
        /// <returns></returns>
        internal async Task RefreshProcessListAsync()
        {
            if (this.IsBusy)
            {
                return;
            }

            ObservableCollection<WmiProcess> newList = null;

            await Task.Factory.StartNew(() =>
                                        {
                                            try
                                            {
                                                var list = new List<WmiProcess>();
                                                var searcher = new ManagementObjectSearcher(GetProcessesQuery);
                                                var collection = searcher.Get();
                                                foreach (var processObject in collection)
                                                {
                                                    var process = new WmiProcess
                                                                  {
                                                                      ProcessName =
                                                                          processObject["Name"].ToStringOrDefault()
                                                                                               .Replace(".exe", ""),
                                                                      StartupArguments =
                                                                          processObject["CommandLine"].ToStringOrDefault
                                                                          (),
                                                                      FullExecutablePath =
                                                                          processObject["ExecutablePath"]
                                                                          .ToStringOrDefault()
                                                                  };
                                                    int tmp;
                                                    int.TryParse(processObject["ProcessId"].ToStringOrDefault(), out tmp);
                                                    process.ProcessId = tmp;
                                                    if (
                                                        process.StartupArguments.StartsWith(process.FullExecutablePath) ||
                                                        process.StartupArguments.StartsWith(string.Format("\"{0}\"",
                                                                                                          process
                                                                                                              .FullExecutablePath)))
                                                    {
                                                        var trimPadding = process.StartupArguments.StartsWith("\"")
                                                                              ? 2
                                                                              : 0;
                                                        process.StartupArguments =
                                                            process.StartupArguments.Substring(
                                                                                               trimPadding +
                                                                                               process
                                                                                                   .FullExecutablePath
                                                                                                   .Length,
                                                                                               process.StartupArguments
                                                                                                      .Length -
                                                                                               process
                                                                                                   .FullExecutablePath
                                                                                                   .Length -
                                                                                               trimPadding).Trim();
                                                    }
                                                    list.Add(process);
                                                }
                                                newList = new ObservableCollection<WmiProcess>(list);
                                            }
                                            catch (Exception)
                                            {
                                                // ToDo: Handle Exception
                                            }
                                        });

            this.RunningProcesses = newList;
        }

        #endregion Internal Methods

        #region Private Fields

        /// <summary>
        ///     Query to get the processes from WMI
        /// </summary>
        private const string GetProcessesQuery =
            "Select ProcessId, CommandLine, ExecutablePath, Name from Win32_Process where ExecutablePath != null";

        private ICommand _abortCommand;

        private Func<Task> _closeAction;

        private bool _isBusy;

        private ICommand _refreshCommand;

        private ObservableCollection<WmiProcess> _runningProcesses;

        private WmiProcess _selectedProcess;

        private ICommand _selectProcessCommand;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        ///     The Bindable Command to Abort the Selection
        /// </summary>
        public ICommand AbortCommand
        {
            get { return this._abortCommand ?? (this._abortCommand = new DelegateCommand(this.Abort)); }
        }

        /// <summary>
        ///     Flag if its refreshing
        /// </summary>
        public bool IsBusy
        {
            get { return this._isBusy; }
            set
            {
                if (this._isBusy != value)
                {
                    this._isBusy = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        ///     Bindable Command to Refresh the TaskList
        /// </summary>
        public ICommand RefreshCommand
        {
            get
            {
                return this._refreshCommand ??
                       (this._refreshCommand = DelegateCommand.FromAsyncHandler(this.RefreshProcessListAsync));
            }
        }

        /// <summary>
        ///     List of all running Processes
        /// </summary>
        public ObservableCollection<WmiProcess> RunningProcesses
        {
            get { return this._runningProcesses; }
            set
            {
                if (this._runningProcesses != value)
                {
                    this._runningProcesses = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// The Selected process 
        /// </summary>
        public WmiProcess SelectedProcess
        {
            get { return this._selectedProcess; }
            set
            {
                if (this._selectedProcess != value)
                {
                    this._selectedProcess = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// The bindable Command to select a process and close the Window
        /// </summary>
        public ICommand SelectProcessCommand
        {
            get
            {
                return this._selectProcessCommand ??
                       (this._selectProcessCommand = new DelegateCommand(this.SelectProcess));
            }
        }

        #endregion Public Properties

        #region Private Methods

        /// <summary>
        /// This Method aborts the dialog by 
        /// setting the selected process to null
        /// and closing the view
        /// </summary>
        private void Abort()
        {
            this.SelectedProcess = null;
            this.Close();
        }

        /// <summary>
        /// This Method closes the view
        /// </summary>
        private void Close()
        {
            this._closeAction.Invoke();
        }


        /// <summary>
        /// This Method selects the process by
        /// simply closing the view
        /// </summary>
        private void SelectProcess()
        {
            this.Close();
        }

        #endregion Private Methods
    }
}
