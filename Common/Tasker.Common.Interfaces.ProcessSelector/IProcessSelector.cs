﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

using System.ComponentModel.Composition;
using System.Threading.Tasks;

namespace Tasker.Common.Interfaces.ProcessSelector
{
    /// <summary>
    /// This Interface defines a class which allows to select a process
    /// </summary>
    [InheritedExport]
    public interface IProcessSelector
    {
        #region Public Methods

        /// <summary>
        /// This Async Method will show a screen to the user to select
        /// a running Process
        /// </summary>
        /// <returns>the selected Process</returns>
        Task<IProcess> SelectProcessAsync();

        #endregion Public Methods
    }
}