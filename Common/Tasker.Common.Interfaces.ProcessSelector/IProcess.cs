﻿#region License

// Copyright (c) 2016 Sebastian S. Schüler
// This file is part of Tasker.
//
//
// Tasker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tasker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tasker.  If not, see <http://www.gnu.org/licenses/>.

#endregion License

namespace Tasker.Common.Interfaces.ProcessSelector
{
    /// <summary>
    /// This interface defines one single Process
    /// </summary>
    public interface IProcess
    {
        #region Public Properties

        /// <summary>
        /// The full path to the file of the Process
        /// </summary>
        string FullExecutablePath { get; }

        /// <summary>
        /// The Process ID
        /// </summary>
        int ProcessId { get; }

        /// <summary>
        /// The Name of the Process
        /// (normally the executable without extension)
        /// </summary>
        string ProcessName { get; }

        /// <summary>
        /// The Arguments of the process which where used
        /// for starting
        /// </summary>
        string StartupArguments { get; }

        #endregion Public Properties
    }
}